import React from "react"
import { Box, Button, Input, FormControl, Select, Text } from "@chakra-ui/core"

const QuickPoll = () => (
  <>
    <Text fontSize="lg" textAlign="center">
      Just One Question:
    </Text>
    <Text mb={6} fontSize="lg" textAlign="center">
      What content is most important to you as a Fault player?
    </Text>
    <form
      action="/feedback-thanks/"
      data-netlify="true"
      name="Quick Poll"
      netlify-honeypot="bot-field"
      method="POST"
    >
      <input type="hidden" name="form-name" value="Quick Poll" />

      <Box display="flex" px={8} width={{ base: "100%", sm: "25rem" }}>
        <FormControl width="100%">
          <Select
            bg="transparent"
            variant="outline"
            name="Content Choice"
            borderColor="yellow.500"
            placeholder="Choose One"
            color="yellow.500"
            mb={4}
            size="lg"
          >
            <option value="video">Curated Steams/Video</option>
            <option value="guides">Hero Build Guides</option>
            <option value="news">Game News</option>
            <option value="articles">Articles/Analysis</option>
            <option value="forums">Discussion Forum</option>
          </Select>
          <Button
            width="100%"
            variantColor="yellow"
            variant="outline"
            type="submit"
          >
            Submit
          </Button>
          <Box style={{ visibility: "hidden" }}>
            If you're a person, ignore this: <input name="bot-field" />
          </Box>
        </FormControl>
      </Box>
    </form>
  </>
)
export default QuickPoll
