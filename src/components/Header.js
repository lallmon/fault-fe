import { Link } from "gatsby"
import { Box, Text } from "@chakra-ui/core"
import PropTypes from "prop-types"
import React from "react"

const Header = ({ siteTitle = "Fault.gg" }) => (
  <Box
    as="header"
    width="100%"
    bg="gray.900"
    color="white"
    height="4rem"
    display="flex"
    alignItems="center"
    position="fixed"
  >
    <Link to="/">
      <Box
        position="absolute"
        display="flex"
        flexDirection="column"
        alignItems="center"
        borderBottomRightRadius={4}
        bg="gray.900"
        px={3}
        top={0}
        w={20}
      >
        <img
          src="https://global-uploads.webflow.com/5d44771a95c1f5121689f944/5e8151fda7eca1d24497e081_FaultLogo_3D.png"
          alt={siteTitle}
        />
        <Text fontSize="sm">.GG</Text>
      </Box>
    </Link>
  </Box>
)
export default Header
