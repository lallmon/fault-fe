import { Link } from "gatsby"
import { Box, Text } from "@chakra-ui/core"
import PropTypes from "prop-types"
import React from "react"

const Footer = ({ siteTitle = "Fault.gg" }) => (
  <Box
    as="footer"
    width="100%"
    bg="gray.900"
    color="white"
    height="4rem"
    display="flex"
    alignItems="center"
    position="fixed"
    justifyContent="center"
    bottom={0}
  >
    <Text mr={1}>Fault © {new Date().getFullYear()}</Text>
    <Text>
      <a href="https://www.playfault.com" target="_blank" rel="noopener">
        Strange Matter Studios
      </a>
    </Text>
  </Box>
)
export default Footer
