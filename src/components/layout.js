/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Header from "./Header"
import Footer from "./Footer"
import { Box, CSSReset, ThemeProvider, Text } from "@chakra-ui/core"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <ThemeProvider>
      <CSSReset />
      <Header siteTitle={data.site.siteMetadata.title} />
      <Box
        as="main"
        py={16}
        h="100vh"
        display="flex"
        flexDirection="column"
        width="100%"
        bg="gray.800"
      >
        {children}
      </Box>
      <Footer />
    </ThemeProvider>
  )
}

export default Layout
