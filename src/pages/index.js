import React from "react"
import { Box, Image, Text } from "@chakra-ui/core"
import QuickPoll from "../components/QuickPoll"
import SEO from "../components/seo"
import Layout from "../components/layout"

const IndexPage = () => (
  <Layout>
    <Box
      color="white"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      height="100%"
      bgRepeat="none"
      bgSize="cover"
      bgPos="right"
      style={{
        backgroundImage: `linear-gradient(
            rgba(0, 0, 0, 0.7),
            rgba(0, 0, 0, 0.7)
          ), url('https://res.cloudinary.com/faultgg/image/upload/q_auto/environment/murdock-jungle.jpg')`,
      }}
    >
      <SEO title="Home" />
      <Text
        textAlign="center"
        mt={16}
        fontWeight="bold"
        textTransform="uppercase"
        color="yellow.500"
        mb={6}
        fontSize={{ sm: "2xl", md: "3xl" }}
      >
        Coming soon, but we need your Input!
      </Text>
      <QuickPoll />
    </Box>
  </Layout>
)

export default IndexPage
