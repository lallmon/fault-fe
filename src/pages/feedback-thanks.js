import React from "react"
import SEO from "../components/seo"
import { Box, Text } from "@chakra-ui/core"
import Layout from "../components/layout"

const FeedbackThanks = () => (
  <Layout>
    <SEO title="Thank You" />
    <Box mt={6}>
      <Text color="white" textAlign="center" as="h2" fontSize="4xl">
        We appreciate your opinion on this!
      </Text>
    </Box>
  </Layout>
)

export default FeedbackThanks
