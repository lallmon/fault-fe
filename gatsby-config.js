module.exports = {
  siteMetadata: {
    title: `Community Site for Fault, a 3rd Person Multiplayer Online Battle Arena`,
    description: `Fault is a MOBA based on the ashes of Paragon By Epic Games. Gameplay and mechanics are the focus to make a competitive multiplayer online battle arena, or MOBA`,
    author: `@anothervenue`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        display: `minimal-ui`,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
